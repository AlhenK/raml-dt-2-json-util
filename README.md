# raml-dt-2-json-util

The Util convert RAML Data Type files from __/types__ directory

to JSON schemas in __/output/schemas__ directory

RAML Data Type file name should be the same as the Type Name which is declared in The RAML

JSON Schema file will have the same name as the corresponded RAML Data Type file name.

To run script install ramldt2jsonschema lib

```
npm install ramldt2jsonschema  js-yaml
```

Then run for RAML to JSON
```
npm run raml2json
```

Or JSON to RAML
```
npm run json2raml
```

RAML should be Library

Make sure the RAML file has a type name
For example:
```
#%RAML1.0 Library 

types:
  book:
  ...
```

Just insert it if no one.

Input file example:

book.raml
```
#%RAML1.0 Library

types:
  book:
    type: object
    properties:
      authors:
        type: array
        items:
          type: !include author.raml
      publisher: !include publisher.raml
```
Result
book.json
```
{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$ref": "#/definitions/book",
  "definitions": {
    "book": {
      "type": "object",
      "additionalProperties": true,
      "required": [
        "authors",
        "publisher"
      ],
      "properties": {
        "authors": {
          "type": "array",
          "items": true
        },
        "publisher": true
      }
    }
  }
}
```


Limitations:

Data type references are not resolved:

RAML 
```
  authors:
    type: array
    items:
      type: !include author.raml
``` 


JSON
```
 "properties": {
   "authors": {
     "type": "array",
     "items": true
 }
```

Solution:

Add all the required references in JSON schema manually:
```
 "properties": {
   "authors": {
     "type": "array",
     "items": { "$ref": "author.json#/definitions/author" }
 }
```




References:
[Github: raml-org/ramldt2jsonschema](https://github.com/raml-org/ramldt2jsonschema)

based on webapi-parser
[Github: webapi-parser](https://raml-org.github.io/webapi-parser/translating-raml-json.html)
