const j2r = require('ramldt2jsonschema')
const join = require('path').join
const fs = require('fs')
const yaml = require('js-yaml')

const jsonDirectoryPath = join(__dirname, '/schemas/')
const files = fs.readdirSync(jsonDirectoryPath);
const ramlTypeDirectoryPath = join(__dirname, '/output/types/')

function main() {
    let jsonData
    files.forEach(jsonFileName => {
        const jsonTypeName = jsonFileName.split('.')[0]
        const jsonFilePath = join(jsonDirectoryPath, jsonFileName)
        const ramlFilePath = join(ramlTypeDirectoryPath, jsonTypeName.concat('.raml'))

        jsonData = fs.readFileSync(jsonFilePath).toString()

        JsonToRamlDataType(jsonData, jsonTypeName, ramlFilePath).catch()
    })
}

async function JsonToRamlDataType(jsonData, typeName, jsonfilePath) {
    let raml
    try {
        raml = await j2r.js2dt(jsonData, typeName)
    } catch (err) {
        console.log(err)
        return
    }
    const ramlData = '#%RAML 1.0 Library\n' + yaml.dump(raml, { 'noRefs': true })
    fs.writeFileSync(jsonfilePath, ramlData)
}

main()
