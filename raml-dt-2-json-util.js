const r2j = require('ramldt2jsonschema')
const join = require('path').join
const fs = require('fs')

const ramlDirectoryPath = join(__dirname, '/types/')
console.log(ramlDirectoryPath)
const files = fs.readdirSync(ramlDirectoryPath);
const jsonSchemaOutputDirectoryPath = join(__dirname, '/output/schemas/')

function main() {
    let ramlData
    files.forEach(ramlFileName => {
        const ramlTypeName = ramlFileName.split('.')[0]
        const ramlFilePath = join(ramlDirectoryPath, ramlFileName)
        const jsonfilePath = join(jsonSchemaOutputDirectoryPath, ramlTypeName.concat('.json'))

        ramlData = fs.readFileSync(ramlFilePath).toString()

        ramlToJsonSchema(ramlData, ramlTypeName, jsonfilePath).catch()
    })
}

async function ramlToJsonSchema(ramlData, ramlTypeName, jsonfilePath) {
    let schema
    try {
        schema = await r2j.dt2js(ramlData, ramlTypeName)
    } catch (err) {
        console.log(err)
        return
    }
    fs.writeFileSync(jsonfilePath, JSON.stringify(schema, null, 2))
}

main()
